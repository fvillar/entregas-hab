"use strict";
/* 
Crear un falso e-commerce. Por un lado tenemos todos los artículos que forman el stock de la tienda con una clase item que consta de nombre del artículo y precio. Tendremos también un usuario que añade cosas a su carrito, que es privado. Los artículos cuando los metemos al carrito los convertimos un tipo de dato que guarda las unidades que tiene el usuario de dicho artículo, lo llamaremos CartItem. La tienda es la encargada de imprimir un ticket por la consola cuando se lo ordena el usuario.

	//Datos de ejemplo para los items
	const itemNames = ['Camisa', 'Pantalon', 'Calcetines'];
		const itemPrices = [13, 27, 100];
*/
//Arrays iniciales de datos
const itemNames = ["Camisa", "Pantalon", "Calcetines", "Zapatos"];
const itemPrices = [13, 27, 100, 200];
//////////////////////////////CLASS ITEM//////////////////////
class Item {
  name;
  price;
  constructor(name, price) {
    this.name = name;
    this.price = price;
  }
}
//////////////////////////////CLASS CARTITEM//////////////////////
class CartItem extends Item {
  units = 1;
  constructor(name, price) {
    super(name, price);
  }
  //Metodo para incrementar una unidad en el producto que ya exite en el carrito
  increase() {
    this.units++;
  }
}
//////////////////////////////CLASS USER//////////////////////
class User {
  #cart = [];
  constructor(name) {
    this.name = name;
  }
  //Metodo para añadir un producto al carrito del usuario
  addToCart(item) {
    for (const pos of this.#cart) {
      if (pos.name === item.name) {
        pos.increase();
        return;
      }
    }
    this.#cart.push(new CartItem(item.name, item.price));
  }
  //Metodo para ejecutar el pago. Llama a un metodo estático de Shop
  pay(user) {
    console.log(`Factura de usuario ${user}:`);
    Shop.checkout(this.#cart);
  }
}
//////////////////////////////CLASS SHOP/////////////////////////////
class Shop {
  catalog = [];
  constructor() {}
  //Metodo para construir inventario a partir de arrays iniciales, instanciando Items en cada posicion del array del inventario
  buildCatalog(items, prices) {
    this.catalog = items.map((item, i) => {
      return new Item(item, prices[i]);
    });
  }
  //Metodo estatico para sacar la factura
  static checkout(carrito) {
    let unidades = 0;
    let precioTotal = 0;
    for (const item of carrito) {
      console.log(
        `Producto: ${item.name} Unidades: ${item.units} Precio unidad: ${
          item.price
        }€ Precio total: ${item.units * item.price}€`
      );
      unidades = unidades + item.units;
      precioTotal = precioTotal + item.units * item.price;
    }
    console.log(`Total unidades: ${unidades} Precio total: ${precioTotal}€`);
    console.log("");
  }
}

////////////////////////////////////////////////////////////
///////////////PRUEBAS DE PROGRAMA/////////////////////////
//////////////////////////////////////////////////////////

//Funcion para crear tienda y su catalogo
function openShop() {
  let shop = new Shop();
  shop.buildCatalog(itemNames, itemPrices);
  return shop;
}

//Creacion y visualizacion de instancia de tienda
let myShop = openShop();
//console.log(myShop);

//creacion de alias para los productos
let [camisa, pantalon, calcetines, zapatos] = myShop.catalog;

//Instancia de usuario
let User1 = new User("User1");
let User2 = new User("User2");

//Funcion para la compra de articulos
function getArticle(user, article) {
  return user.addToCart(article);
}

//Compra de articulos
getArticle(User1, camisa);
getArticle(User1, camisa);
getArticle(User1, camisa);
getArticle(User1, pantalon);
getArticle(User1, calcetines);
getArticle(User1, pantalon);
getArticle(User1, calcetines);
getArticle(User2, pantalon);
getArticle(User2, calcetines);
getArticle(User2, pantalon);
getArticle(User2, calcetines);
getArticle(User2, pantalon);
getArticle(User2, calcetines);
getArticle(User2, zapatos);
getArticle(User2, zapatos);

//Mostar usuario y su carrito
//console.log(User1);
//console.log(User2);

//Mostar el total de productos y de precio.
User1.pay("User1");
User2.pay("User2");
