"use strict";
//Accede con fetch a la información de la siguiente API: https://api.exchangerate-api.com/v4/latest/EUR. Dado un valor en euros (EUR), convierte dicha cantidad a dólares (USD). Por último convierte esta cantidad en dólares a yenes (JPY).
const urlEuro = "https://api.exchangerate-api.com/v4/latest/EUR";
const urlDolar = "https://api.exchangerate-api.com/v4/latest/USD";

//pedir con fetch, sacar cambio de dolar y yen, hacer calculos.
async function cambioEuroDolarYen(ammount) {
  //peticion inicial a servidor, que nos devuelve una promesa
  const peticionJSONCambioEuroDolar = await fetch(urlEuro);
  //recibimos un archivo json y lo transformamos en algo más usable
  const dataCambioEuroDolar = await peticionJSONCambioEuroDolar.json();
  //obtenemos el dato concreto que nos interesa por destructuring de objetos
  const { USD } = dataCambioEuroDolar.rates;
  //Repetimos todo el proceso anterior con una nueva url, pero esta vez obtenemos el dato concreto que nos interesa directamente del objeto
  const peticionJSONCambioDolarYen = await fetch(urlDolar);
  const dataCambioDolarYen = await peticionJSONCambioDolarYen.json();
  const JPY = dataCambioDolarYen.rates.JPY;
  //calculamos los cambios y los mostramos
  console.log(
    ammount +
      " euros son " +
      ammount * USD +
      "dolares, y " +
      ammount * USD +
      " dolares son " +
      ammount * USD * JPY +
      "yenes"
  );
}

cambioEuroDolarYen(5);
