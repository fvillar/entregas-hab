"use strict";
/* 
Haz que la función PalindromeTwo(str) tome el parámetro str que se le pasa y devuelva true si el parámetro es un palíndromo, (la cadena se lee igual hacia adelante que hacia atrás) de lo contrario devuelve false. Por ejemplo: "Arriba la birra" debería devolver true, se lee igual del derecho que del revés.
*/

function adaptarString(str) {
  let strLower = str.toLowerCase();
  //Eliminamos todo lo que no sean letras o numeros en el retorno
  return strLower.replace(/[^A-Za-z0-9]/g, "");
}

function esPalindromo(str) {
  for (let i = 0; i < str.length / 2; i++) {
    if (str[i] !== str[str.length - 1 - i]) {
      return false;
    }
  }
  return true;
}

function PalindromeTwo() {
  let str = prompt("Introduce la frase que desees analizar", "Arriba la birra");
  let getLetras = adaptarString(str);
  return console.log(esPalindromo(getLetras));
}

PalindromeTwo();
