/* 
Haz que la función BinaryConverter(str) devuelva la forma decimal del valor binario. Por ejemplo: si se pasa 101 el programa debe retornar un 5, si se pasa 1000 debe retornar un 8, etc. Si no sabes como convertir un número binario a decimal puedes echar un ojo a este vídeo: https://www.youtube.com/watch?v=bBMhiSy1Grc
*/
"use strict";
function validateBinaryString(str) {
  for (let i = 0; i < str.length; i++) {
    if (str[i] !== "0" && str[i] !== "1") {
      return false;
    }
  }
  return true;
}

function stringToBinary(str) {
  let binario = [];
  for (let i = 0; i < str.length; i++) {
    binario[i] = +str[i];
  }
  return binario;
}
function calculateDecimalFromBinary(binaryNumber) {
  let decimalNumber = 0;
  for (let i = 0; i < binaryNumber.length; i++) {
    decimalNumber =
      decimalNumber + binaryNumber[i] * 2 ** (binaryNumber.length - 1 - i);
  }
  return decimalNumber;
}

function BinaryConverter() {
  let str = prompt("¿De qué número binario quieres saber el valor?", "1000");
  if (validateBinaryString(str)) {
    let binaryNumber = stringToBinary(str);
    return console.log(calculateDecimalFromBinary(binaryNumber));
  } else {
    console.log(
      `La secuencia introducida tiene algún caracter no valido. Recuerda que solo puedes poner ceros y unos.`
    );
  }
}

BinaryConverter();
