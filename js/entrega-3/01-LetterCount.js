"use  strict";
/* 
Dada la función LetterCount(str) toma el parámetro str que se está pasando y devuelve la primera palabra de mayor longitud. Por ejemplo: Hoy es un día estupendo y fantástico. debe devolver fantástico porque es la primera palabra que más letras tiene.
*/

function splitInWords(str) {
  return str.split(/[ .,]/);
}

function maxLettersWord(words) {
  let maxLetters = 0;
  let index;
  for (let i = 0; i < words.length; i++) {
    if (words[i].length > maxLetters) {
      maxLetters = words[i].length;
      index = i;
    }
  }
  return words[index];
}

function LetterCount() {
  let str = prompt(
    "Introduce la frase que desees analizar",
    "Hoy es un día estupendo y fantástico"
  );
  let words = splitInWords(str);
  return console.log(maxLettersWord(words));
}

LetterCount();
