"use strict";

/* Crea un programa que simule el comportamiento de un dado.El programa debe ir almacenando los valores de cada tirada en una variable, y cuando llegue a 50 puntos o más, debe finalizar su ejecución mostrando un mensaje tal que:

¡Enhorabuena, ha salido un Y! ¡Has ganado con un total de Z puntos!.

A su vez, debe mostrarse el siguiente mensaje tras cada tirada:

Tirada X: ha salido un Y.Tienes un total de Z puntos.

(X: número de la tirada, Y: número aleatorio del 1 al 6, Z: total de puntos acumulados).

    P.D: X, Y, Z son nombres que he dado para explicar lo que pido, no quiere decir que debáis llamar de esa manera a las variables correspondientes. */

function tirarDado() {
  return Math.ceil(Math.random() * 6);
}

function jugarDados() {
  let numeroTirada = 0;
  let valorTirada = 0;
  let sumaValorTiradas = 0;
  while (sumaValorTiradas < 50) {
    valorTirada = tirarDado();
    numeroTirada++;
    sumaValorTiradas = sumaValorTiradas + valorTirada;

    if (sumaValorTiradas < 50) {
      console.log(
        `Tirada ${numeroTirada}: Ha salido un ${valorTirada}. Tienes un total de ${sumaValorTiradas} puntos`
      );
    } else {
      console.log(
        `¡Enhorabuena, ha salido un ${valorTirada}! ¡Has ganado con un total de ${sumaValorTiradas} puntos!.`
      );
    }
  }
}

jugarDados();
