"use strict";

/* Calcula la media de puntos en los últimos tres partidos de estos tres equipos y muestra por consola el que tenga una media más alta:
Equipo María: 62, 34, 55.
Equipo Paula: 35, 60, 59.
Equipo Rebeca: 40, 39, 63. */
function calcularMedia(puntos1, puntos2, puntos3) {
  return (+puntos1 + +puntos2 + +puntos3) / 3;
}

function crearEquipo(nombre, partido1, partido2, partido3) {
  const equipo = {
    name: nombre,
    puntosp1: +partido1,
    puntosp2: +partido2,
    puntosp3: +partido3
  };
  equipo.mediaPuntos = calcularMedia(
    equipo.puntosp1,
    equipo.puntosp2,
    equipo.puntosp3
  );
  return equipo;
}

function crearListaEquipos() {
  const lista = {
    a: crearEquipo("Equipo Maria", 62, 34, 55),
    b: crearEquipo("Equipo Paula", 35, 60, 59),
    c: crearEquipo("Equipo Rebeca", 40, 39, 63)
  };
  return lista;
}

function encontrarMayorMedia(listaEquipos) {
  let mayorMedia = 0;
  if (listaEquipos.a.mediaPuntos > mayorMedia) {
    mayorMedia = listaEquipos.a.mediaPuntos;
  }
  if (listaEquipos.b.mediaPuntos > mayorMedia) {
    mayorMedia = listaEquipos.b.mediaPuntos;
  }
  if (listaEquipos.c.mediaPuntos > mayorMedia) {
    mayorMedia = listaEquipos.c.mediaPuntos;
  }
  return mayorMedia;
}

function equipoMediaMasAlta() {
  const ligaDia = crearListaEquipos();
  if (ligaDia.a.mediaPuntos === encontrarMayorMedia(ligaDia)) {
    console.log(
      `El equipo con media mas alta es ${ligaDia.a.name} con una media de ${ligaDia.a.mediaPuntos} por partido`
    );
  }
  if (ligaDia.b.mediaPuntos === encontrarMayorMedia(ligaDia)) {
    console.log(
      `El equipo con media mas alta es ${ligaDia.b.name} con una media de ${ligaDia.b.mediaPuntos} por partido`
    );
  }
  if (ligaDia.c.mediaPuntos === encontrarMayorMedia(ligaDia)) {
    console.log(
      `El equipo con media mas alta es ${ligaDia.c.name} con una media de ${ligaDia.c.mediaPuntos} por partido`
    );
  }
}

equipoMediaMasAlta();
