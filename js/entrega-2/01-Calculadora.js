"use strict";

/* En el mismo repositorio que habéis creado para la primera entrega deberéis crear la carpeta JS a la misma altura que la carpeta HTML.Dentro de la carpeta creada(JS), debéis crear otra carpeta más llamada entrega - 2. Ahí debéis compartir la solución(index.js) con el ejercicio resuelto.La entrega constará de varios ejercicios que se irán entregando según vaya avanzando la semana, comienzo proponiendo el primero de ellos:

Crea una calculadora por medio de if... ...else y switch (de las dos formas) que opere con dos números.Debe ser capaz de sumar, restar, multiplicar, dividir y elevar el número uno a la potencia de número dos.En caso de que el usuario introduzca un tipo de operación no contemplada por el programa deberá mostrarse un mensaje indicando que no se ha seleccionado un tipo de operación correcta. */

function getNumber() {
  return +prompt("Introduzca un numero");
}

function action() {
  return +prompt("1 suma, 2 resta, 3 multiplicacion, 4 division, 5 potencia");
}

function calculadoraif() {
  let firstNumber = getNumber();
  let secondNumber = getNumber();
  let codeAction = action();
  if (codeAction === 1) {
    return alert(firstNumber + secondNumber);
  } else if (codeAction === 2) {
    return alert(firstNumber - secondtNumber);
  } else if (codeAction === 3) {
    return alert(firstNumber * secondNumber);
  } else if (codeAction === 4) {
    return alert(firstNumber / secondNumber);
  } else if (codeAction === 5) {
    return alert(firstNumber ** secondNumber);
  } else {
    return alert("La operacion solicitada no es correcta");
  }
}

calculadoraif();

function calculadoraSwitch() {
  let firstNumber = getNumber();
  let secondNumber = getNumber();
  let codeAction = action();
  switch (codeAction) {
    case 1:
      return alert(firstNumber + secondNumber);
      break;
    case 2:
      return alert(firstNumber - secondNumber);
      break;
    case 3:
      return alert(firstNumber * secondNumber);
      break;
    case 4:
      return alert(firstNumber / secondNumber);
      break;
    case 5:
      return alert(firstNumber ** secondNumber);
      break;
    default:
      return alert("La operacion solicitada no es correcta");
      break;
  }
}

calculadoraSwitch();
