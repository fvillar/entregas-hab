const minimist = require("minimist");
const {
  addTodo,
  markAsDone,
  markAsUnDone,
  listTodos,
  cleanTodos,
  resetTodoList,
  eraseOneTask,
} = require("./lib/actions");

const { _, priority, list, done, undone, clean, reset, eraseone } = minimist(
  process.argv.slice(2)
);

//console.log(_, priority, list, done, undone, clean, reset, eraseone);
//console.log(process.argv.slice(2));

//Procesamos arguments

if (done === 0) {
  console.error(`El numero de tarea 0 no existe.`);
  process.exit();
}

if (done) {
  markAsDone({ index: done });
  process.exit();
}

if (undone === 0) {
  console.error(`El numero de tarea 0 no existe.`);
  process.exit();
}

if (undone) {
  markAsUnDone({ index: undone });
  process.exit();
}

if (list) {
  listTodos();
  process.exit();
}

if (clean) {
  cleanTodos();
  process.exit();
}

if (reset) {
  resetTodoList();
  process.exit();
}

if (eraseone === 0) {
  console.error(`El numero de tarea 0 no existe.`);
  process.exit();
}

if (eraseone) {
  eraseOneTask({ index: eraseone });
  process.exit();
}

addTodo({
  text: _.join(" "),
  priority,
});
