const { format } = require("date-fns");
const { es, gl } = require("date-fns/locale");
const json = require("./tasks.json");
const fs = require("fs-extra");
const chalk = require("chalk");
const path = require("path");

function addTodo({ text, priority }) {
  if (text !== "") {
    //Generamos la tarea completa
    const task = {
      text: text,
      added: new Date(),
      priority: priority ? "high" : "normal",
      done: false,
    };
    //Introducimos la tarea en el listado
    json.tasks.push(task);
    //Salvamos el listado en archivo JSON
    saveTodosToFile();
  }
}

function markAsDone({ index }) {
  const array = json.tasks;
  //Comprobamos que el index es valido
  if (index > 0 && index <= array.length) {
    //Ponemos a true el valor de index-1 para ajustar el index de la tarea al del array
    array[index - 1].done = true;
    //Salvamos en fichero
    saveTodosToFile();
  } else {
    console.error(`El numero de tarea indicado es erroneo.`);
  }
}

function markAsUnDone({ index }) {
  const array = json.tasks;
  //Comprobamos que el index es valido
  if (index > 0 && index <= array.length) {
    //Ponemos a false el valor de index-1 para ajustar el index de la tarea al del array
    array[index - 1].done = false;
    //Salvamos en fichero
    saveTodosToFile();
  } else {
    console.error(`El numero de tarea indicado es erroneo.`);
  }
}

function listTodos() {
  const array = json.tasks;
  //Comprobamos si la lista de tareas está vacia
  if (array.length === 0) {
    console.log(`La lista de tareas está vacia`);
  } else {
    for (let i = 0; i < array.length; i++) {
      const numero = i + 1;
      const fecha = format(
        new Date(array[i].added),
        "EEEE, d 'de' MMMM 'de' yyyy",
        { locale: es }
      );
      const prioridad = array[i].priority;
      const texto = array[i].text;
      const estado = array[i].done;
      if (estado === true) {
        console.log(
          chalk`{grey ${numero} ${texto} ${prioridad} ${fecha}}`,
          chalk.green("Finalizada")
        );
      }
      //Respuesta si la tarea está sin hacer y con prioridad alta
      if (estado === false && prioridad === "high") {
        console.log(
          chalk`{white ${numero}} {cyan ${texto}} {red ${prioridad}} ${fecha}`,
          chalk.yellow("Pendiente")
        );
      }
      //Respuesta si la tarea está sin hacer y con prioridad normal
      if (estado === false && prioridad === "normal") {
        console.log(
          chalk`{white ${numero}} {cyan ${texto}} {green ${prioridad}} ${fecha}`,
          chalk.yellow("Pendiente")
        );
      }
    }
  }
}

function cleanTodos() {
  const array = json.tasks;
  const length = array.length;
  //Comprobamos si la lista está vacia
  if (length === 0) {
    console.log(`La lista de tareas está vacía`);
  } else {
    //Filtramos array para quedarnos con los datos que nos interesan
    const undones = array.filter((task) => task.done === false);
    const undonesLength = undones.length;
    //Calculamos los elementos eliminados para informarlo despues
    const borrados = length - undonesLength;
    json.tasks = undones;
    //Configuramos mensaje del resultado
    if (borrados === 0) {
      console.log(`No hay ninguna tarea lista para borrar`);
    } else {
      console.log(`Se han borrado ${borrados} tareas`);
    }
    //Salvamos en fichero
    saveTodosToFile();
  }
}

function saveTodosToFile() {
  //Configuramos la fecha de modificacion del archivo
  json.lastUpdate = new Date();

  //Guardamos datos en archivo JSON de manera sincrona
  fs.writeFileSync("./lib/tasks.json", JSON.stringify(json));
  console.log(`La modificacion se ha realizado con exito`);
}

//OPCION ASINCRONAS:
// CON CALLBACK

/* function saveTodosToFile() {
  json.lastUpdate = new Date();
  fs.writeFile(
    path.join(__dirname, "./tasks.json"),
    JSON.stringify(json),
    function (error) {
      if (error) console.error(error);
      console.log(`La modificacion se ha realizado con exito`);
    }
  );
} */

//CON ASYNC AWAY

/* async function saveTodosToFile() {
  json.lastUpdate = new Date();
  try {
    await fs.writeFile(
      path.join(__dirname, "./tasks.json"),
      JSON.stringify(json)
    );
    console.log(`La modificacion se ha realizado con exito`);
  } catch (error) {
    console.error(error);
  }
} */

// Con guardado asincrono, dan error los if del todo.js porque antes de que se guarde el archivo, se ejecuta el process.exit()

//EXTRAS

//BORRADO COMPLETO
function resetTodoList() {
  json.tasks = [];
  saveTodosToFile();
}

//BORRADO DE UNA TAREA CONCRETA
function eraseOneTask({ index }) {
  const array = json.tasks;
  console.log(index - 1);
  console.log(array.length);
  array.splice(index - 1, 1);
  saveTodosToFile();
}

module.exports = {
  addTodo,
  markAsDone,
  markAsUnDone,
  listTodos,
  cleanTodos,
  resetTodoList,
  eraseOneTask,
};
