const os = require("os");
const chalk = require("chalk");

const totalram = os.totalmem();
const freeram = os.freemem();
const notuseram = (freeram / totalram) * 100;

console.log("\n", "RAM total: ", chalk.blue(totalram));
console.log(" RAM libre: ", chalk.yellow(freeram));
if (notuseram > 10) {
  console.log(
    " Tienes disponible el",
    chalk.green(notuseram.toFixed(2)),
    "% de la ram."
  );
} else {
  console.log(
    " Tienes disponible el",
    chalk.red(notuseram.toFixed(2)),
    "% de la ram."
  );
}
