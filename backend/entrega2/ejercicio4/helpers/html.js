function pageLayout(title, content) {
  return `
  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>${title}</title>
    
    <link rel="stylesheet" href="/css/style.css" />
  </head>
  <body>
    <header>
      <h1><a href="/">Pokedex!</a></h1>
    </header>

      ${content}
  
    </body>
  </html>
  `;
}

function frontPage() {
  return `
  <section class=formulario>
    <form action="/search" method="GET">
      <fieldset>
        <label for="query">Busca tu pokemon</label>
        <input type="search" name="query" id="query" /> 
      </fieldset>
      <button>Buscar</button>
    </form>
  </section>
  `;
}

function searchResults(results) {
  if (results.length === 0) return errorPage("No results");

  const htmlResults = results.map((result) => {
    const { english, japanese } = result.name;

    return `<li>
              <a href="/pokemon/${result.id}">
                ${english} / ${japanese}
              </a>
              (${result.type.join(", ")})
            </li>`;
  });

  return `
  <section class=resultados>
   <ul>
    ${htmlResults.join("")}
   </ul>
  </section>
  `;
}

function errorPage(message) {
  return `
    <section class="error">
      <p>${message}</p>
      <a href="/">Volver a la portada</a>
    </section>
  `;
}

function pokemonCard(pokemon) {
  const { english, japanese } = pokemon.name;
  const type = pokemon.type.join(",");
  const { HP, Attack, Defense, Speed } = pokemon.base;
  const SpAttack = pokemon.base["Sp. Attack"];
  const SpDefense = pokemon.base["Sp. Defense"];

  const imageFile = image(pokemon.id.toString());

  return `
  <section class="pokemonCard">
    <h1> ${english} / ${japanese} </h1>
    <img src="/images/${imageFile}" alt="imagen ${english} / ${japanese}" />
    <ul>
      <li>Tipo:${type}</li>
      <li>HP:${HP}</li>
      <li>Attack:${Attack}</li>
      <li>Defense:${Defense}</li>
      <li>Sp. Attack:${SpAttack}</li>
      <li>Sp. Defense:${SpDefense}</li>
      <li>Speed:${Speed}</li>
    </ul>
  </section>
  `;
}

function image(id) {
  if (id.length === 1) {
    id = `00${id}`;
  } else if (id.length === 2) {
    id = `0${id}`;
  } else {
  }
  return `/${id}.png`;
}

module.exports = {
  pageLayout,
  frontPage,
  errorPage,
  searchResults,
  pokemonCard,
};
