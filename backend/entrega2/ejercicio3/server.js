require("dotenv").config();
const express = require("express");
const app = express();
const port = process.env.PORT;

app.get("/", function (req, res, next) {
  const hour = new Date().getHour();
  console.log(hour);
  if (hour > 13) {
    next();
  }
  res.status(200).send("Hola");
});

app.use((error, req, res, next) => {
  res.status(500).send("Error al ejecutar una ruta");
});

app.use((req, res, next) => {
  res.status(404).send("Not found");
});

app.listen(port, () => {
  console.log(`Servidor iniciado en puerto ${port}`);
});
