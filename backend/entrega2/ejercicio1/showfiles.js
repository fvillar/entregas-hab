const minimist = require("minimist");
const fs = require("fs-extra");
const args = minimist(process.argv.slice(2));
const path = require("path");
const chalk = require("chalk");

const files = args._;

async function printIfLowerTenK({ files }) {
  const largeFileNames = files.map((file) => path.resolve(file));

  for (const fileName of largeFileNames) {
    try {
      const stats = await fs.stat(fileName);
      const size = stats.size;
      if (size < 10000) {
        const texto = (await fs.readFile(fileName)).toString();
        console.log("\n", "  - ", chalk.green("Mostrar"), chalk.grey(fileName));
        console.log(`\n ${texto}`);
      } else {
        console.log(
          "\n",
          "  - ",
          chalk.yellow("Mayor 10kB"),
          chalk.grey(fileName)
        );
      }
    } catch (error) {
      //console.error("\n", "  - ", chalk.red("No existe"), chalk.grey(fileName));
      console.error(chalk`\n   -  {red No existe} {grey ${fileName}}`);
    }
  }
}

printIfLowerTenK({ files });
