"use strict";
const abrCoin = {
  EUR: "Euros",
  AED: "Dirhams-Emiratos",
  ARS: "Pesos-Argentinos",
  AUD: "Dolares-Australianos",
  BGN: "Levs-Bulgaros",
  BRL: "Reales-Brasileños",
  BSD: "Dolares-Bahameños",
  CAD: "Dolsres-Canadienses",
  CHF: "Francos-Suizos",
  CLP: "Pesos-Chilenos",
  CNY: "Yuanes-Chinos",
  COP: "Pesos-Colombianos",
  CZK: "Coronas-Checas",
  DKK: "Coronas-Danesas",
  DOP: "Pesos-Dominicanos",
  EGP: "Libras-Egipcias",
  FJD: "Dolares-de-Fiji",
  GBP: "Libras-Esterlinas",
  GTQ: "Quetzales-Guatemaltecos",
  HKD: "Dolares-Hong-Kong",
  HRK: "Krunas-Croatas",
  HUF: "Forintos-Hungaros",
  IDR: "Rupias-Indonesias",
  ILS: "Sequeles-Israelis",
  INR: "Rupias-Indias",
  ISK: "Coronas-Islandesas",
  JPY: "Yenes-Japonese",
  KRW: "Wons-Surcoreanos",
  KZT: "Tenges-de-Kazajistan",
  MXN: "Pesos-Mexicanos",
  MYR: "Ringgits-de-Malasia",
  NOK: "Coronas-Noruegas",
  NZD: "Dolares-de-Nueva-Zelanda",
  PAB: "Balboas-Panameños",
  PEN: "Nuevos-Soles-Peruanos",
  PHP: "Pesos-Filipinos",
  PKR: "Rupias-Pakistanis",
  PLN: "Zlotys-Polacos",
  PYG: "Guaranis-Paraguayos",
  RON: "Nuevos-Leis-Rumanos",
  RUB: "Rublos-Rusos",
  SAR: "Rials-Saudis",
  SEK: "Coronas-Suecas",
  SGD: "Dolares-de-Singapur",
  THB: "Bahts-Tailandeses",
  TRY: "Nuevas-Liras-Turcas",
  TWD: "Dolares-Taiwaneses",
  UAH: "Hryvnias-Ucranianos",
  USD: "Dolares-Estadounidenses",
  UYU: "Pesos-Uruguayos",
  ZAR: "Rands-Sudafricanos"
};

const urlGeneral = "https://api.exchangerate-api.com/v4/latest/";
const urlEuro = "https://api.exchangerate-api.com/v4/latest/EUR";
const initialCoinDataList = document.querySelector("datalist#monedaInicial");
const finalCoinDataList = document.querySelector("datalist#monedaFinal");

async function obtenerMonedas(datalist) {
  const JSONdata = await fetch(urlEuro);
  const data = await JSONdata.json();
  const coins = Object.keys(data.rates);
  for (const coin of coins) {
    const option = document.createElement("option");
    option.setAttribute("value", coin);
    datalist.append(option);
  }
}

function getData() {
  const ammount = document.querySelector("input#ammount").value;
  const initialCoin = document.querySelector("input#initialCoin").value;
  const finalCoin = document.querySelector("input#finalCoin").value;
  return [ammount, initialCoin, finalCoin];
}

async function cambioMoneda() {
  const [ammount, initialCoin, finalCoin] = getData();
  const url = urlGeneral + initialCoin;
  const JSONdata = await fetch(url);
  const data = await JSONdata.json();
  const resultado = (ammount * data.rates[finalCoin]).toFixed(2);
  return (
    ammount +
    " " +
    abrCoin[initialCoin] +
    " son " +
    resultado +
    " " +
    abrCoin[finalCoin]
  );
}

async function insertarResultado() {
  const main = document.querySelector("main");
  main.style.textAlign = "center";
  const h3Resultado = document.querySelector("main>h3");
  h3Resultado.textContent = await cambioMoneda();
  h3Resultado.style.cssText = "color:#ccab5a;";
  main.append(h3Resultado);
}

function handleClick(event) {
  event.preventDefault();
  if (document.querySelector("main>h3")) {
    const h3 = document.querySelector("main>h3");
    h3.textContent = "";
  } else {
    const main = document.querySelector("main");
    const h3 = document.createElement("h3");
    main.append(h3);
  }
  insertarResultado();
  document.querySelector("input#initialCoin").value = "";
  document.querySelector("input#finalCoin").value = "";
  document.querySelector("input#ammount").value = "";
}

const body = document.querySelector("body");
body.addEventListener("load", obtenerMonedas(initialCoinDataList));
body.addEventListener("load", obtenerMonedas(finalCoinDataList));

const imagen = document.querySelector("input#calcular");
calcular.addEventListener("click", handleClick);
