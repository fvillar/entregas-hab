//Importamos los datos de acceso a la api del archivo config.js que hemos creado
import config from "./config.js";
//Importamos axios despues de instalarlo por consola con npm -i --save axios
const axios = require("axios").default;
//Creamos una constante donde guardar nuestra apiKey publica
const apiKey = config.apiKey;
//Creamos una constante con la url base y otra con la parte de geolocalizacion de la api a la que vamos a llamar para conseguir la lista de artistas. En esta url se incorpora nuestra constante en la que guardamos nuestra apiKey publica
const BASE_URL = "https://ws.audioscrobbler.com/";
const URL_GEO =
  "2.0/?method=geo.gettopartists&country=spain&limit=50&api_key=" +
  apiKey +
  "&format=json";

//Creamos contantes para el resto de llamadas a las api

const URL_TOP_TRACKS =
  "2.0/?method=geo.gettoptracks&country=spain&limit=50&api_key=" +
  apiKey +
  "&format=json";
const URL_TOP_TAGS =
  "2.0/?method=chart.gettoptags&limit=50&api_key=" + apiKey + "&format=json";

//Creamos funcion asincrona para coger topartist de lastfm españa
async function getArtist() {
  try {
    const response = await axios.get(`${BASE_URL}${URL_GEO}`);
    return response;
  } catch (error) {
    console.error(error);
  }
}
//Creamos funcion asincrona para coger toptracks de lastfm españa
async function getTopTracks() {
  try {
    const response = await axios.get(`${BASE_URL}${URL_TOP_TRACKS}`);
    return response;
  } catch (error) {
    console.error(error);
  }
}
//Creamos funcion asincrona para coger toptags de lastfm españa
async function getTopTags() {
  try {
    const response = await axios.get(`${BASE_URL}${URL_TOP_TAGS}`);
    return response;
  } catch (error) {
    console.error(error);
  }
}

//Configuramos la exportacion de las funciones
export default {
  getArtist,
  getTopTracks,
  getTopTags,
};
