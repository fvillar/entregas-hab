import Vue from "vue";
import App from "./App.vue";
import router from "./router";
/* Importamos vue-headful despues de instalarlo para poder cambiar el nombre de la pagina al cambiar de vista */
import vueHeadful from "vue-headful";

Vue.config.productionTip = false;
/* Declaramos el componente vue-headful*/
Vue.component("vue-headful", vueHeadful);

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
