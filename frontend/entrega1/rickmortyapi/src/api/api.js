const axios = require("axios").default;
const apiUrl = "https://rickandmortyapi.com/api";

function getAll() {
  return axios.get(`${apiUrl}/character`);
}

function getChar(id) {
  return axios.get(`${apiUrl}/character/${id}`);
}

export default {
  getAll,
  getChar,
};
