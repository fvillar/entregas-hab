import Vue from "vue";
import VueRouter from "vue-router";
/* Importamos Home de una de las dos formas posibles. 
Esta forma es en dos pasos, primero importamos la vista en la parte
superior del archivo y despues la asociamos a su ruta en las constantes(lineas 11-14) */
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  /* Importamos about de la segunda forma posible de importacion. En esta ocasion lo 
  hacemos en un solo paso, haciendo la importacion a través de un arrow function dentro de la 
  definicion de la constante de la ruta */
  {
    path: "/about",
    name: "About",
    component: () => import("../views/About.vue"),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
