//Importando lo que necesito para la conexion/Api
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const mysql = require("mysql");

//JSONWEBTOKEN DEPENDENCIAS
const jwt = require("jsonwebtoken");
const config = require("./config.js");

//Declaro la APP
const app = express();

//APP USES
app.use(cors());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());
app.set("llave", config.llave);

//DATOS DE CONEXION A LA BBDD
const connection = mysql.createConnection({
  host: "localhost",
  user: "felix",
  password: "felixvillar",
  database: "hackamarket",
});

//CONEXION CON LA BASE DE DATOS
connection.connect((error) => {
  if (error) throw error;
  console.log("DATABASE UP");
});

//PUERTO CONEXION DE LA API
const PORT = 3050;

//Creamos servidor
app.listen(PORT, () => console.log(`API UP in port ${PORT}`));

//FUNCION PARA CREAR NUEVO CLIENTE
app.post("/clientes/add", (req, res) => {
  //SECUENCIA SQL
  const sql = "INSERT INTO clientes SET ?";
  const newClient = {
    nombre: req.body.nombre,
    apellido: req.body.apellido,
    ciudad: req.body.ciudad,
    empresa: req.body.empresa,
  };
  //CONEXION
  connection.query(sql, newClient, (error) => {
    //si sale mal
    if (error) throw error;
    res.send("Cliente creado/a");
  });
});

//FUNCION PARA CREAR NUEVO USUARIO
app.post("/usuarios/add", (req, res) => {
  const newUser = {
    email: req.body.email,
    password: req.body.password,
    admin: req.body.admin,
  };
  //SECUENCIA SQL PARA COMPROBAR QUE NO EXISTE EL EMAIL EN BBDD
  const sqlExistEmail = `SELECT id FROM usuarios WHERE email='${newUser.email}'`;
  connection.query(sqlExistEmail, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.status(409).send("duplicado");
    } else {
      //SECUENCIA SQL PARA INSERTAR
      const sql = "INSERT INTO usuarios SET ?";
      //CONEXION
      connection.query(sql, newUser, (error) => {
        //si sale mal
        if (error) throw error;
        res.send("Usuario/a creado/a");
      });
    }
  });
});

//FUNCION PARA RECUPERAR TODOS LOS CLIENTES DE LA BASE DE DATOS
app.get("/clientes", (req, res) => {
  //SECUENCIA SQL
  const sql = "SELECT * FROM clientes";
  //CONEXION
  connection.query(sql, (error, results) => {
    //si sale mal
    if (error) throw error;
    //si hay resultados
    if (results.length > 0) {
      res.json(results);
    }
    //si no hay nada
    else {
      res.send("La lista de clientes está vacía");
    }
  });
});

//FUNCION PARA ACTUALIZAR UN CLIENTE
app.put("/clientes/update/:id", (req, res) => {
  /* Datos que nos llegan */
  const id = req.body.id;
  const nombre = req.body.nombre;
  const apellido = req.body.apellido;
  const ciudad = req.body.ciudad;
  const empresa = req.body.empresa;
  const sql = `UPDATE clientes SET nombre='${nombre}', apellido='${apellido}', ciudad='${ciudad}', empresa='${empresa}' WHERE id='${id}'`;
  connection.query(sql, (error) => {
    if (error) throw error;
    res.send("Cliente actualizado/a");
  });
});

//FUNCION PARA BORRAR CLIENTES
app.delete("/clientes/del/:id", (req, res) => {
  //SECUENCIA SQL
  const id = req.params.id;
  const sql = `DELETE FROM clientes WHERE id=${id}`;
  //CONEXION
  connection.query(sql, (error) => {
    //si sale mal
    if (error) throw error;
    res.send("Cliente borrado/a");
  });
});

//FUNCION PARA RECUPERAR TODOS LOS PRODUCTOS DE LA BASE DE DATOS
app.get("/productos", (req, res) => {
  //SECUENCIA SQL
  const sql = "SELECT * FROM productos";
  //CONEXION
  connection.query(sql, (error, results) => {
    //si sale mal
    if (error) throw error;
    //si hay resultados
    if (results.length > 0) {
      res.json(results);
    }
    //si no hay nada
    else {
      res.send("La lista de productos está vacía");
    }
  });
});

//FUNCION PARA RECUPERAR UN PRODUCTO DE LA BASE DE DATOS POR ID
app.get("/productos/:id", (req, res) => {
  const id = req.params.id;
  //SECUENCIA SQL
  const sql = `SELECT * FROM productos WHERE id=${id}`;
  //CONEXION
  connection.query(sql, (error, results) => {
    //si sale mal
    if (error) throw error;
    //si hay resultados
    if (results.length > 0) {
      res.json(results);
    }
    //si no hay nada
    else {
      res.send("No existe ese producto");
    }
  });
});

//METODO LOGIN QUE CREA EL TOKEN
app.post("/auth", (req, res) => {
  /* DATOS QUE LLEGAN(USER Y PASSWORD) */
  const email = req.body.email;
  const password = req.body.password;
  //SECUENCIA SQL
  const sql = `SELECT * FROM usuarios WHERE email='${email}' AND password='${password}'`;
  //CONEXION A LA BBDD
  connection.query(sql, (error, results) => {
    let admin = null;
    if (error) throw error;
    if (results.length) {
      const payload = {
        check: true,
      };
      if (results[0].admin) {
        admin = true;
      } else {
        admin = false;
      }
      const token = jwt.sign(payload, app.get("llave"), {
        expiresIn: "1 day",
      });
      res.json({
        mensaje: "Te has autenticado corectamente",
        token: token,
        Admin: admin,
        email: results[0].email,
      });
    } else {
      console.log("Datos incorrectos");
      res.status(401).send("error");
    }
  });
});
