import axios from "axios";
import jwt from "jwt-decode";

const ENDPOINT = "http://localhost:3050";
const AUTH_TOKEN_KEY = "authToken";
const ROLE = "role";
const EMAIL = "email";

//FUNCION DE LOGIN
export function loginUser(email, password) {
  return new Promise(async (resolve, reject) => {
    try {
      let res = await axios({
        url: `${ENDPOINT}/auth`, //URL DE LA AUTENTICACION
        method: "POST", //METODO DE LA AUTENTICACION
        data: {
          email: email,
          password: password,
          grant_type: "password",
        }, //DATOS DE LA AUTENTICACION
      });
      setAuthToken(res.data.token);
      setAdmin(res.data.Admin);
      setUserEmail(res.data.email);
      resolve();
    } catch (err) {
      console.log("Error en login:", err);
      reject(err);
    }
  });
}

//LOGOUT
export function clearLogin() {
  axios.defaults.headers.common["Authorization"] = "";
  localStorage.removeItem(AUTH_TOKEN_KEY);
  clearAdmin();
  clearEmail();
}

//GUARDAR TOKEN EN LOCALSTORAGE
export function setAuthToken(token) {
  axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  localStorage.setItem(AUTH_TOKEN_KEY, token);
}

//Guardar si es admin en local storage
export function setAdmin(Admin) {
  localStorage.setItem(ROLE, Admin);
}

//Guardar email en local storage
export function setUserEmail(email) {
  localStorage.setItem(EMAIL, email);
}

//Borrar rol del user en localstorage
export function clearAdmin() {
  return localStorage.removeItem(ROLE);
}

//Borrar rol del user en localstorage
export function clearEmail() {
  return localStorage.removeItem(EMAIL);
}

//Recuperar rol desde el localstorage
export function getAdmin() {
  return localStorage.getItem(ROLE);
}
//Recuperar email desde el localstorage
export function getEmail() {
  return localStorage.getItem(EMAIL);
}

//Comprobar si el rol es admin o no
export function checkAdmin() {
  let role = false;
  let Admin = getAdmin();
  if (Admin === "true") {
    role = true;
  }
  return role;
}

//COMPROBAR Y DEVOLVER FECHA EXPIRACION TOKEN
export function getTokenExpirationDate(encodedToken) {
  let token = jwt(encodedToken);
  //SI NO HAY, NO SIGUE
  if (!token.exp) {
    return null;
  }
  let date = new Date(0);
  date.setUTCSeconds(token.exp);
  return date;
}

//COMPROBAMOS SI ES VALIDA LA FECHA DEL TOKEN
export function isTokenExpired(token) {
  let expirationDate = getTokenExpirationDate(token);
  return expirationDate < new Date();
}

//COGER EL TOKEN DEL LOCAL STORAGE
export function getAuthToken() {
  return localStorage.getItem(AUTH_TOKEN_KEY);
}

//COMPROBAR SI EL USER ESTÁ LOGUEADO O NO
export function isLoggedIn() {
  let authToken = getAuthToken();
  return !!authToken && !isTokenExpired(authToken);
}
