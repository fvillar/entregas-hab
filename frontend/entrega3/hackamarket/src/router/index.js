import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login.vue";
import { isLoggedIn } from "../api/utils";
import { checkAdmin } from "../api/utils";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Login",
    component: Login,
    meta: {
      //RUTA PUBLICA
      allowAnonymous: true,
    },
  },
  {
    path: "/about",
    name: "About",
    component: () => import("../views/About.vue"),
    meta: {
      //RUTA PRIVADA
      allowAnonymous: false,
    },
  },
  {
    path: "/add-clients",
    name: "AddClients",
    component: () => import("../views/AddClients.vue"),
    meta: {
      //RUTA PRIVADA
      allowAnonymous: false,
      allowNoAdmin: false,
    },
    BeforeEnter: (to, from, next) => {
      if (to.meta.allowNoAdmin === false && !checkAdmin()) {
        next({
          path: "/productos",
          query: { redirect: to.fullPath },
        });
      } else {
        next();
      }
    },
  },
  {
    path: "/clientes",
    name: "Clientes",
    component: () => import("../views/Clientes.vue"),
    meta: {
      //RUTA PRIVADA
      allowAnonymous: false,
    },
    BeforeEnter: (to, from, next) => {
      if (to.meta.allowNoAdmin === false && !checkAdmin()) {
        next({
          path: "/clientes",
          query: { redirect: to.fullPath },
        });
      } else {
        next();
      }
    },
  },
  {
    path: "/productos",
    name: "Productos",
    component: () => import("../views/Productos.vue"),
    meta: {
      //RUTA PRIVADA
      allowAnonymous: false,
    },
  },
  {
    path: "/register",
    name: "Register",
    component: () => import("../views/Register.vue"),
    meta: {
      //RUTA PRIVADA
      allowAnonymous: true,
    },
  },
  {
    path: "/*",
    name: "Error",
    component: () => import("../views/Error.vue"),
    meta: {
      //RUTA PRIVADA
      allowAnonymous: true,
    },
  },
];

const router = new VueRouter({
  routes,
});

//COMPROBANDO CADA PÁGINA POR SI LA PERSONA ESTÁ LOGUEADA
router.beforeEach((to, from, next) => {
  //SI LA RUTA ES PRIVADA Y LA PERSONA NO TIENE TOKEN
  if (!to.meta.allowAnonymous && !isLoggedIn()) {
    next({
      path: "/",
      query: { redirect: to.fullPath },
    });
  } else {
    next();
  }
});

export default router;
